# ics-ans-role-imagej

Ansible role to install [ImageJ](https://imagej.nih.gov/ij/index.html), including the ADViewers plugins.

## Role Variables

See: [Default variables](defaults/main.yml)

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-imagej
```

## License

BSD 2-clause
