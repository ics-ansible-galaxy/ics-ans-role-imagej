import os
import testinfra.utils.ansible_runner
import pytest

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")

IMAGEJ_HOME = "/opt/ImageJ"
CONDA_ENV = "/opt/conda/envs/adviewer"


def test_imagej_installed(host):
    imagej_run = host.file(os.path.join(IMAGEJ_HOME, "run"))
    assert imagej_run.is_file
    assert imagej_run.mode == 0o755
    assert host.file(os.path.join(IMAGEJ_HOME, "ij.jar")).exists


@pytest.mark.parametrize(
    "plugin",
    [
        "EPICS_areaDetector/EPICS_AD_Viewer.java",
        "EPICS_areaDetector/EPICS_AD_Viewer.class",
        "EPICS_areaDetector/EPICS_NTNDA_Viewer.java",
        "EPICS_areaDetector/EPICS_NTNDA_Viewer.class",
    ],
)
def test_plugins_installed(host, plugin):
    assert host.file(os.path.join(IMAGEJ_HOME, "plugins", plugin)).exists


@pytest.mark.parametrize("lib", ["libblosc.so", "libjpeg.so", "libz.so"])
def test_adviewer_required_libs_installed(host, lib):
    assert host.file(os.path.join(CONDA_ENV, "lib", lib)).exists
